% %12
% fun = @root2d;
% x0 = [1.2,1.2];
% x = fsolve(fun,x0)
% 
% function F = root2d(x)
% F(1) = x(2)+x(1).^2-x(1)-0.75;
% F(2) = x(2)+5.*x(1).*x(2)-x(1).^2;
% end


% %13
% x = 10.*randn(1000,1) + 70;
% h = histogram(x);


% %14
% t = [10 15 20 25 40 50 55 60 75];
% y = [5 20 18 40 33 54 70 60 78];
% p = polyfit(t,y,1);
% res = polyval(p,32)
% 
% %绘点
% plot(t, y, 'o')
% xlabel("t")
% ylabel("y")
% hold on
% %绘制直线
% y2 = p(1) .* t + p(2);
% plot(t, y2)
% scatter(32, res,"*","r")
% hold off


% %18发福爹
% f = @(t,x,a,b,r) [-a.*x(1)+a.*x(2);r.*x(1)-x(2)-x(1).*x(3);-b.*x(3)+x(1).*x(2)];
% h=0.03125;a=10;b=8/3;r=28;tspan=[0 20];x=[5 5 5];
% options=odeset('MaxStep',h);
% [t,y]=ode45(f,tspan,x,options,a,b,r);
% 
% x1=[5 5 5.001];
% [t1,y1]=ode45(f,tspan,x1,options,a,b,r);
% % subplot(2,2,1);plot(y(:,1),y(:,2));
% % subplot(2,2,2);plot(y(:,2),y(:,3));
% % subplot(2,2,3);plot(y(:,1),y(:,3));
% % subplot(2,2,4);plot3(y(:,1),y(:,2),y(:,3));grid;
% 
% subplot(2,1,1);plot(t,y(:,1),'g');
% hold on
% subplot(2,1,1);plot(t1,y1(:,1),'r');
% hold off
% subplot(2,1,2);plot(y(:,1),y(:,3),'g');
% hold on
% subplot(2,1,2);plot(y1(:,1),y1(:,3),'r');
% hold off


% %15
% x=linspace(1.8,2.6,5);
% j=[0.5815 0.5767 0.556 0.5202 0.4708];
% j1=interp1(x,j,2.1,'spline')
% j2=interp1(x,j,2.1,'pchip')


% %16
% f = @(x,y) x.^2-3.*y.^2+x.*y.^3;
% q1=integral2(f,0,4,-2,2)
% x=linspace(0,4,400);y=linspace(-2,2,400);
% [X,Y]=meshgrid(x,y);
% z=f(X,Y);
% q2=trapz(y,trapz(x,z,2))


%17
f=@(t,y) [y(2);(1-y(1).^2).*y(2)-y(1)];
t=[0 10];y0=[1 1];
[t,y]=ode45(f,t,y0);
plot(t,y);