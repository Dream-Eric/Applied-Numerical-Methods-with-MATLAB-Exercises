x=linspace(1,4,40);y=linspace(-2,0,40);
[X,Y] = meshgrid(x,y);
Z=-8*X+X.^2+12*Y+4*(Y.^2)-2*X.*Y
subplot(1,2,1);
cs=contour(X,Y,Z);clabel(cs);
xlabel('x_1');ylabel('x_2');title('(a) Contour plot');grid;
subplot(1,2,2);
cs=surfc(X,Y,Z);
zmin=floor(min(Z));
zmax=ceil(max(Z));
xlabel('x_1');ylabel('x_2');zlabel('f(x_1,x_2)');
title('(b)Mesh plot');

%f=@(x) -8*x(1)+x(1).^2+12*x(2)+4*(x(2).^2)-2*x(1)*x(2);[x,fval]=fminsearch(f,[-0.5,0.5])