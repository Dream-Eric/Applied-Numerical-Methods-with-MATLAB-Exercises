%Name:        exercise7_14.m
%Author:      Shi Yuke
%Created:     2022-11-12
%Version:     1.0.0
x = linspace(-0.8, -0.55, 100);
y = linspace(0.15, 0.5, 100);
[X, Y] = meshgrid(x, y);
Z =  1 ./ (1 + X.^2 + Y.^2 + X + X .* Y);

subplot(1, 2, 1)
cs = contour(X, Y, Z);
clabel(cs);
xlabel('x_1');ylabel('x_2');
title('(a) Contour plot');
grid;

subplot(1, 2, 2);
cs = surfc(X, Y, Z);
zmin = floor(min(Z));
zmax = ceil(max(Z));
xlabel('x_1');ylabel('y_1');zlabel('f(x_1, x_2)');
title('(b) Mesh plot');

f = @(x) -1 / (1 + x(1)^2 + x(2)^2 + x(1) + x(1) * x(2));
[xmin, fval] = fminsearch(f, [0, 0]);
xmax = xmin
fval = -fval


