function [x,fx,ea,iter] = goldmin(f,xl,xu,es,maxit,varargin)
% [x,fx,ea,iter]=goldmin(f,xl,xu,es,maxit,pl,p2,...):
% uses golden section search to find the minimum of f 
% input:
%   f = name of function
%   xl,xu = lower and upper guesses
%   es = desired relative error(default = 0.0001%)
%   maxit = maximum allowable iterations(default = 50)
%   pl,p2,.. = additional parameters used by f %
% output:
%  x = location of minimum %  fx = minimum function value
%  ea = approximate relative error(%)
%  iter = number of iterations

if nargin<3,error('at least 3 input arguments required'),end 
if nargin<4|isempty(es),es=0.0001;end 
if nargin<5|isempty(maxit),maxit=50;end 
phi=(1+sqrt(5))/2; iter=0;
d = (phi-1)*(xu - xl);
xl = xl + d;x2 = xu - d;
f1 = f(xl,varargin{:});f2=f(x2,varargin{:});
while(1)
   xint = xu - xl;
   if f1 < f2
      xopt = xl;xl = x2;x2 = xl;f2 = f1;
      xl = xl + (phi-1)*(xu-xl); f1 = f(xl,varargin{:});
   else 
      xopt = x2;xu = xl; xl = x2; f1 = f2;
      x2 = xu-(phi-1)*(xu-xl);f2 = f(x2,varargin{:});
   end
   iter=iter+1;
   if xopt~=0, ea = (2 - phi) * abs(xint/xopt) * 100;end 
   if ea <= es|iter >= maxit,break,end 
end
x=xopt;fx=f(xopt,varargin{:});
%g = 9.81;v0=60;m=90;c=15;z0=0;
%z = @(t)-(z0+m/c*(v0+m*g/c)*(1-exp(-c/m*t))-m*g/c*t);
%[xmin,fmin,ea]=goldmin(z,0,8)