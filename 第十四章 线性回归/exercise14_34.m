%Name:        exercise14_34.m
%Author:      CaptianIceBear
%Created:     2022-11-13
%Version:     1.0.0


clc,format short g
n = 10000; B = 20; H = 3;
nm = 0.03; nmmin = nm - 0.003, nmmax = nm + 0.003;
S = 0.0003;Smin = S - 0.003, Smax = S + 0.003;
r = rand(n,1)
nmrand = nmmin+(nmmax-nmmin)*r;
meannm = mean(nmrand),stdnm = std(nmrand)
Deltanm = (max(nmrand)-min(nmrand))/meannm/2*100;
subplot(311)
hist(nmrand),title('(a) Distribution of Roughness')
xlabel('Ra')
Srand = Smin+(Smax-Smin)*r;
meanS = mean(Srand),stdS = std(Srand)
DeltaS = (max(Srand)-min(Srand))/meanS/2*100;
subplot(312)
hist(Srand),title('(a) Distribution of slope')
Qrand = (1/nm)*(((B*H).^(5/3))/(B+2*H).^(2/3))*sqrt(S)
meanQ  = mean(Qrand)
DeltaQ = (max(Qrand)-min(Qrand))/meanQ/2*100;
subplot(313)
hist(nmrand),title('(a) Distribution of flow')
xlabel('m^3/s')
