%Name:        exercise14_1.m
%Author:      Shi Yuke
%Created:     2022-11-12
%Version:     1.0.0
a = [0.90 1.42 1.30 1.55 1.63 ...
1.32 1.35 1.47 1.95 1.66 ...
1.96 1.47 1.92 1.35 1.05 ...
1.85 1.74 1.65 1.78 1.71 ...
2.29 1.82 2.06 2.14 1.27];
mean_a = mean(a)
median_a = median(a)
mode_a = mode(a)
range = max(a) - min(a)
std_a = std(a)
var_a = var(a)
cv = std_a / mean_a