%Name:        exercise14_2.m
%Author:      Shi Yuke
%Created:     2022-11-13
%Version:     1.0.0
x = [88.9 108.5 104.1 139.7 127 94 116.8 99.1];
v = [14.6 16.7 15.3 23.2 19.5 16.1 18.1 16.1];

%绘点
plot(x, v, 'o')
xlabel("Precipitation/(cm)")
ylabel("Velocity/(m^3/s)")
hold on

%线性拟合
p = polyfit(x, v, 1);

%绘制直线
y = p(1) .* x + p(2);
plot(x, y)
hold off

%降水量120cm流速
v_120 = p(1) .* 120 + p(2)

%假定降水量以12小时划分
water = 1100 * 10^3 * 120 * 10^-2;
real = v_120 * 12 * 60 * 60;
waste = (water - real) / water